<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUrlsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::create('urls', function (Blueprint $table) {
			$table->increments('id');
			$table->string('url', 2000);
			$table->string('short', 25)->unique();
			$table->enum('type', ['default', 'custom'])->default('default');
			$table->integer('clicks')->default(0);
			$table->integer('shorten_retries')->default(0);
			$table->timestamps();
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::dropIfExists('urls');
    }
}
