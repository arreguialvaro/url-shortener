<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Faker\Generator;

class ShortenUrlTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
		for($i = 0; $i < 5000; $i++){
			$response = $this->json('POST', '/urls', ['url' => "http://{$i}.com"]);
			$response->assertStatus(302);
		}

    }
}
