<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Url;

class UrlController extends Controller
{
	/**
	 * Return a list of the urls that have been created
	 */
	public function index()
	{
		$urls = Url::orderBy('id', 'desc')->paginate(10);

		return view('urls.index', ['urls' => $urls, 'paginate' => true]);
	}

	/**
	 * Store a new URL
	 *
	 * @param Request $request
	 */
	public function store(Request $request)
	{
		$this->validate($request, [
			'url' => 'required|url',
			'custom' => 'nullable|alpha_num|max:25'
		]);

		$type = (isset($request->custom)) ? 'custom' : 'default';

		switch ($type){
			case 'custom':
				if($url = Url::where(['short' => $request->custom])->first()){
					return redirect('/')->withFlashDanger('This vanity URL is already taken!');
				}

				$short = $request->custom;
				break;
			default:
				if($url = Url::where(['url' => $request->url, 'type' => 'default'])->first()){
					$url->shorten_retries++;
					$url->save();

					return redirect('/')->withFlashInfo('URL has been shortened before!')->with(['retry' => true, 'short' => $url->short]);
				}

				//$short = uniqid();
				$short = $this->shortUrlGenerator();
				break;
		}


		$url = new Url;
		$url->url = $request->url;
		$url->short = $short;
		$url->type = $type;

		$url->save();

		return redirect('/')->withFlashSuccess('URL shortened successfully!');
	}

	/**
	 * Redirect the user to the requested URL
	 *
	 * @param $short
	 */
	public function redirect($short)
	{
		if(!$url = Url::where('short', $short)->first()){
			return redirect('/')->withFlashDanger('URL not found, try again!');
		}

		$url->clicks++;
		$url->save();

		return redirect($url->url);
	}

	public function top()
	{
		$urls = Url::orderBy('clicks', 'desc')->orderBy('id', 'desc')->limit(5)->get();

		return view('urls.index', ['urls' => $urls, 'paginate' => false]);
	}

	private function shortUrlGenerator()
	{
		$retry = 0;

		do{
			$hash = hash('sha256', microtime());
			$hash = base_convert($hash, 16, 36);
			if($retry > 0){
				\Log::debug("Trying again: {$retry}");
			}
			$retry++;
			$length = rand(5, 8);
			$short = substr($hash, 0, $length);
			$url = Url::where('short', $short)->first();
		}while($url);

		return $short;
	}
}
