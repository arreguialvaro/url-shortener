<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Default route
Route::get('/', 'UrlController@index');

//Restful API, for the moment only the index and store method are required
Route::resource('urls', 'UrlController', ['only' => ['index', 'store']]);

//Get the top 5 clicked URLS
Route::get('urls/top', 'UrlController@top');

//Redirect route
Route::get('{short}', 'UrlController@redirect');

