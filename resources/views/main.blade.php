<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="google" content="notranslate">
	<meta http-equiv="Content-Language" content="es">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<title>Wizeline - ShortUrl</title>
	<!-- Tell the browser to be responsive to screen width -->
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

	<link rel="stylesheet" href="{{ asset('css/app.css') }}">


	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
	<script src="{{asset('js/app.js')}}"></script>
</head>
<body>

<nav class="navbar navbar-default navbar-fixed-top">
	<div class="container">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			<a class="navbar-brand" href="/">Wizeline ShortUrl</a>
		</div>

		<div class="collapse navbar-collapse">
			<ul class="nav navbar-nav">
				<li><a href="/urls/top">Top 5 Clicks</a></li>
			</ul>
		</div><!-- /.navbar-collapse -->
	</div>
</nav>

<main>
	@yield('content')
</main>


</body>
</html>
