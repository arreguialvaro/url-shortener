@extends('main')
@section('content')
	<div class="container">
		@include('_partials.messages')
		{{ Form::open(['url' => 'urls', 'mothod' => 'post']) }}
			<div class="form-group{!! ($errors->has('url')) ? ' has-error' : '' !!}">

				{{ Form::text('url', null, ['class' => 'form-control', 'placeholder' => 'URL to shorten']) }}
			</div>

			<div class="form-group{!! ($errors->has('custom')) ? ' has-error' : '' !!}">

				<div class="input-group">
					<span class="input-group-addon" id="vanity">{{ url('/')  }}/</span>
					{{ Form::text('custom', null, ['class' => 'form-control', 'aria-describedby' => 'vanity', 'placeholder' => 'YourVanityUrl']) }}
				</div>
			</div>


			{{Form::submit('Shorten!', ['class' => 'btn btn-primary pull-right'])}}


		{{ Form::close() }}
		<table class="table table-striped">
			<thead>
			<tr>
				<th>URL</th>
				<th>Short</th>
				<th>Cllicks</th>
				<th>Shorten Retries</th>
				<th>Creation Date</th>
			</tr>
			</thead>
			<tbody>
			@foreach($urls as $url)
				<tr>
					<td>{{$url->url}}</td>
					<td>
						<a href="{{ url('/') }}/{{ $url->short }}" target="_blank" id="{{ $url->id }}">
							{{$url->short}}
						</a>&nbsp;&nbsp;
						<button class="btn btn-sm btn-primary pull-right" data-clipboard-text="{{ url('/') }}/{{ $url->short }}">
							<span class="glyphicon glyphicon-copy"></span>
						</button>
					</td>
					<td>{{$url->clicks}}</td>
					<td>{{$url->shorten_retries}}</td>
					<td>{{$url->created_at}}</td>
				</tr>
			@endforeach
			</tbody>
		</table>
		@if($paginate == true)
			<div class="text-center">
				{{ $urls->links() }}
			</div>
		@endif
	</div>
@endsection