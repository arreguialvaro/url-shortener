# Wizeline ShortURL

Simple service for URL shortening.

##Quickstart

Go to your project directory

```shell
cd /absolute/path/to/project
```

Install PHP dependecies
```shell
composer install
```

Create your enviroment variables and your `APP_KEY`
```shell
cp .env.example .env
php artisan key:generate
```

After setting up your database credentials in your `.env` file, create the tables needed for the project
```shell
php artisan migrate
```

Run Laravels built-in server
```shell
php artisan serve
```

Navigate to [http://localhost:8000](http://localhost:8000)


The CSS & JS files are ready for production. If you need the un-minified/un-compressed versions you can use:
```shell
npm install
npm run dev
```